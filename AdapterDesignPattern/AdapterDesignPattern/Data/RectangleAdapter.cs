﻿namespace AdapterDesignPattern.Data
{
    public class RectangleAdapter : IShape
    {

        private readonly Rectangle Adaptee;

        public RectangleAdapter(Rectangle adaptee)
        {
            this.Adaptee = adaptee;
        }

        public string Draw(int a, int b, int c, int d)
        {
            int x = Math.Min(a, c);
            int y = Math.Min(b, d);
            int width = Math.Abs(c - a);
            int height = Math.Abs(d - b);
            return Adaptee.Draw(x, y, width, height);
        }
    }
}
