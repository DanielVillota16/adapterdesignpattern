﻿namespace AdapterDesignPattern.Data
{
    public interface IShape
    {
        string Draw(int a, int b, int c, int d);
    }
}
