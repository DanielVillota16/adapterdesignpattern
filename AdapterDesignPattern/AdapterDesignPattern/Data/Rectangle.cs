﻿namespace AdapterDesignPattern.Data
{
    public class Rectangle
    {
        public string Draw(int x, int y, int width, int height)
        {
            return "Rectangle with coordinate left-down point (" + x + ";" + y + "), width: " + width + ", height: " + height;
        }
    }
}
