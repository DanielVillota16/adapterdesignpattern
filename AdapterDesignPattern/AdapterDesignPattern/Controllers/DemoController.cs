using AdapterDesignPattern.Data;
using Microsoft.AspNetCore.Mvc;

namespace AdapterDesignPattern.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DemoController : ControllerBase
    {
        RectangleAdapter Adapter = new RectangleAdapter(new Rectangle());

        private readonly ILogger<DemoController> _logger;

        public DemoController(ILogger<DemoController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetAdapterDemo")]
        public IActionResult Get()
        {
            int x1=1, y1=2, x2=3, y2=4;
            return Ok(Adapter.Draw(x1, y1, x2, y2));
        }
    }
}