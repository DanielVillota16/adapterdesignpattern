# Adapter Design Pattern

This is a simple .NET API that internally uses the Adapter design pattern.

# Run the endpoint

## Run the next command
```
dotnet run
```

Then go to: http://localhost:{port}/swagger/index.html

The terminal is going to indicate in its logs the **port** number that the application is using.

## Or you can open the project in Visual Studio 2022 and run it there.
